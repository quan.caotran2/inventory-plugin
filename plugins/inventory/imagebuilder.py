import boto3

from ansible.plugins.inventory import BaseInventoryPlugin

ANSIBLE_METADATA = {
    'metadata_version': '1.0.0',
    'status': ['preview'],
    'supported_by': 'community'
}

DOCUMENTATION = '''
---
module: imagebuilder
plugin_type: inventory
short_description: Ansible Inventory Plugin for AppStream ImageBuilder
version_added: "2.9.13"
description:
    - "A Inventory Plugin created for AppStream ImageBuilder."
options:
'''


class InventoryModule(BaseInventoryPlugin):
    """A trivial example of an inventory plugin."""

    NAME = 'aws.appstream.imagebuilder'

    def verify_file(self, path):
        """Verify that the source file can be processed correctly.

        Parameters:
            path:AnyStr The path to the file that needs to be verified

        Returns:
            bool True if the file is valid, else False
        """
        # Unused, always return True
        return True

    def _get_raw_host_data(self):
        """Get the raw static data for the inventory hosts

        Returns:
            dict The host data formatted as expected for an Inventory Script
        """

        data =  {
            "all": {
                "hosts": []
            },
            "_meta": {
                "hostvars": {}
            }
        }

        appstream = boto3.client('appstream', 'us-east-1')
        instances = appstream.describe_image_builders()

        hostvars = {}
        for instance in instances["ImageBuilders"]:
            if instance["State"] == "RUNNING":
                data["all"]["hosts"].append(instance["NetworkAccessConfiguration"]["EniPrivateIpAddress"])
                hostvars[instance["NetworkAccessConfiguration"]["EniPrivateIpAddress"]] = {"ansible_user": "remote_admin"}
        data["_meta"]["hostvars"] = hostvars

        return data

    def _populate(self):
        appstream = boto3.client('appstream', 'us-east-1')
        instances = appstream.describe_image_builders()

        for instance in instances["ImageBuilders"]:
            if instance["State"] == "RUNNING":
                hostname = instance["NetworkAccessConfiguration"]["EniPrivateIpAddress"]
                tags = appstream.list_tags_for_resource(ResourceArn=instance["Arn"])
                if len(tags["Tags"].items()) != 0:
                    for var_key, var_val in tags["Tags"].items():
                        group=f"tag_{var_key}_{var_val}"
                        self.inventory.add_group(group)
                        self.inventory.add_child('all', group)
                        self.inventory.add_host(hostname,group)
                else:
                    self.inventory.add_host(hostname)


    def parse(self, inventory, *args, **kwargs):
        """Parse and populate the inventory with data about hosts.

        Parameters:
            inventory The inventory to populate

        We ignore the other parameters in the future signature, as we will
        not use them.

        Returns:
            None
        """
        # The following invocation supports Python 2 in case we are
        # still relying on it. Use the more convenient, pure Python 3 syntax
        # if you don't need it.
        super(InventoryModule, self).parse(inventory, *args, **kwargs)

        # self.inventory.add_group('image_builder')
        # self.inventory.add_child('all', 'image_builder')

        # raw_data = self._get_raw_host_data()
        # _meta = raw_data.pop('_meta')
        # for group_name, group_data in raw_data.items():
        #     for host_name in group_data['hosts']:
        #         self.inventory.add_host(host_name, 'image_builder')
        #         for var_key, var_val in _meta['hostvars'][host_name].items():
        #             self.inventory.set_variable(host_name, var_key, var_val)

        self._populate()
